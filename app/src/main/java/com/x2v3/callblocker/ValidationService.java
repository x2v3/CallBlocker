package com.x2v3.callblocker;

import android.app.Notification;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.android.internal.telephony.ITelephony;
import com.x2v3.callblocker.filters.INumberFilter;
import com.x2v3.callblocker.filters.RegexFilter;

import java.lang.reflect.Method;
import java.util.ArrayList;

/**
 * validate phone number
 * Created by x on 2017/8/31.
 */

public class ValidationService extends Service
{

    private final MyBinder myBinder = new MyBinder();
    private final CallBroadCastReceiver receiver = new CallBroadCastReceiver();
    private final Notification notification = new Notification();

    @Nullable
    @Override
    public IBinder onBind(Intent intent)
    {
        return myBinder;
    }

    @Override
    public void onCreate()
    {
        loadFilters();
        receiverInit();

        notification.tickerText = "CallBlocker";
        startForeground(2333, notification);
    }

    /**
     * 初始化广播
     */
    private void receiverInit()
    {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(TelephonyManager.ACTION_PHONE_STATE_CHANGED);
        registerReceiver(receiver, intentFilter);
        Log.d("FILTER", "CallBroadCastReceiver registered.");
    }

    @Override
    public void onDestroy()
    {
        unregisterReceiver(receiver);
        Log.d("FILTER", "CallBroadCastReceiver unregistered.");
        stopForeground(true);
    }


    class MyBinder extends Binder
    {
        ValidationService getService()
        {
            return ValidationService.this;
        }
    }


    public ArrayList<INumberFilter> getFilters()
    {
        if (filters == null)
        {
            loadFilters();
        }
        return filters;
    }

    private ArrayList<INumberFilter> filters;
    private static ArrayList<String> whiteList;

    public boolean FilterIncomingCalls = true;

    private void loadFilters()
    {
        //TODO load filters dynamically.
        RegexFilter rf = new RegexFilter();
        rf.setPatternString("^17\\d+$");
        this.filters = new ArrayList<>();
        filters.add(rf);
    }

    private boolean checkNumber(String number, boolean contactAsWhiteList, Context context)
    {
        if (!FilterIncomingCalls)
        {
            return false;
        }
        if (contactAsWhiteList && getWhiteList(context).contains(number))
        {
            Log.d("checkNumber", "the incoming number is in white list:" + number);
            return false;
        }
        for (INumberFilter f : this.getFilters())
        {
            if (f.IsBlocked(number))
            {
                Log.d("checkNumber", "blocked");
                return true;
            }
        }
        return false;
    }

    public ArrayList<String> getContactNumbers(Context context)
    {
        ArrayList<String> contactList = new ArrayList<>();
        String phoneNumber;
        Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        String _ID = ContactsContract.Contacts._ID;
        String HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;
        Uri PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        String NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;
        ContentResolver contentResolver = context.getContentResolver();
        Cursor cursor = contentResolver.query(CONTENT_URI, null, null, null, null);
        // Iterate every contact in the phone
        if (cursor.getCount() > 0)
        {
            while (cursor.moveToNext())
            {
                String contact_id = cursor.getString(cursor.getColumnIndex(_ID));
                int hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndex(HAS_PHONE_NUMBER)));
                if (hasPhoneNumber > 0)
                {
                    //This is to read multiple phone numbers associated with the same contact
                    Cursor phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", new String[]{contact_id}, null);
                    while (phoneCursor.moveToNext())
                    {
                        phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndex(NUMBER));
                        phoneNumber = phoneNumber.replaceAll("[\\s\\-]+", "");
                        Log.v("ContactNumber", phoneNumber);
                        contactList.add(phoneNumber);
                    }
                    phoneCursor.close();
                }
            }
        }
        return contactList;
    }

    private ArrayList<String> getWhiteList(Context context)
    {
        if (whiteList == null)
        {
            whiteList = getContactNumbers(context);
        }
        return whiteList;
    }

    /**
     * 监听来电事件的广播
     */
    public class CallBroadCastReceiver extends BroadcastReceiver
    {

        @Override
        public void onReceive(Context context, Intent intent)
        {
            TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);

            try
            {
                Class c = Class.forName(tm.getClass().getName());
                Method m = c.getDeclaredMethod("getITelephony");
                m.setAccessible(true);
                ITelephony telephonyService = (ITelephony) m.invoke(tm);
                Bundle bundle = intent.getExtras();
                String phoneNumber = bundle.getString("incoming_number");
                Log.d("INCOMING", phoneNumber);
                if (FilterIncomingCalls && checkNumber(phoneNumber, true, context))
                {
                    telephonyService.endCall();
                    Log.d("HANG UP", phoneNumber);
                } else
                {
                    Log.d("PASSED", "filter status:" + (FilterIncomingCalls ? "on" : "off"));
                }

            } catch (Exception e)
            {
                e.printStackTrace();
            }
        }
    }
}
