package com.x2v3.callblocker;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Switch;
import android.widget.Toast;

import com.x2v3.callblocker.filters.INumberFilter;
import com.x2v3.callblocker.filters.LocationFilter;
import com.x2v3.callblocker.filters.RegexFilter;

/**
 * 主界面（虽然大家都知道 但是我还是要注释）
 */
public class MainActivity extends AppCompatActivity implements
        View.OnClickListener, CompoundButton.OnCheckedChangeListener, TextWatcher
{

    private ValidationService validationService;

    private Switch regexSwitch;
    private Switch locationSwitch;
    private Switch filterSwitch;
    private EditText txtRegex;
    private EditText txtLocation;
    private ListView lv_filterNumbers;          //详细号码忽略列表 TODO

    /**
     * 服务连接回调
     */
    ServiceConnection connection = new ServiceConnection()
    {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder)
        {
            MainActivity.this.validationService = ((ValidationService.MyBinder) iBinder).getService();
            Toast.makeText(MainActivity.this, "ServiceConnected", Toast.LENGTH_SHORT).show();
            setControls();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName)
        {
            MainActivity.this.validationService = null;
            Toast.makeText(MainActivity.this, "ServiceDisconnected", Toast.LENGTH_SHORT).show();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //布局初始化
        viewInit();
        //开启服务
        bindValidationService();
        //申请权限
        RequestPermission();
    }

    /**
     * 布局初始化
     */
    private void viewInit()
    {
        Button button = (Button) findViewById(R.id.btnPermission);
        button.setOnClickListener(this);

        filterSwitch = (Switch) findViewById(R.id.filterSwitch);
        filterSwitch.setOnCheckedChangeListener(this);

        regexSwitch = (Switch) findViewById(R.id.regexSwitch);
        regexSwitch.setOnCheckedChangeListener(this);

        locationSwitch = (Switch) findViewById(R.id.switchLocation);
        locationSwitch.setOnCheckedChangeListener(this);

        txtLocation = (EditText) findViewById(R.id.txtLocation);
        txtLocation.addTextChangedListener(this);

        txtRegex = (EditText) findViewById(R.id.regexPattern);
        txtRegex.addTextChangedListener(this);
    }

    @Override
    protected void onStop()
    {
        super.onStop();
        if (validationService != null)
            unbindService(connection);
    }

    /**
     * 开启服务
     */
    private void bindValidationService()
    {
        Intent intent = new Intent(this, ValidationService.class);
        //TODO 你到底是要绑定服务还是独立开启服务啊。。。。
        startService(intent);
        bindService(intent, connection, Context.BIND_AUTO_CREATE);
    }

    public void RequestPermission()
    {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_CONTACTS, Manifest.permission.CALL_PHONE},
                1);
    }

    private void setControls()
    {
        filterSwitch.setChecked(validationService.FilterIncomingCalls);
        for (INumberFilter f : this.validationService.getFilters())
        {
            if (f.getClass() == LocationFilter.class)
            {
                LocationFilter locationFilter = (LocationFilter) f;
                locationSwitch.setChecked(locationFilter.enabled);
            } else if (f.getClass() == RegexFilter.class)
            {
                RegexFilter regexFilter = (RegexFilter) f;
                regexSwitch.setChecked(regexFilter.enabled);
                txtRegex.setText(regexFilter.getPatternString());
            }
        }
    }

    @Override
    public void onClick(View view)
    {
        RequestPermission();
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b)
    {
        if (this.validationService == null)
        {
            Toast.makeText(this, "Error:service is not connected.", Toast.LENGTH_LONG).show();
            return;
        }
        switch (compoundButton.getId())
        {
            case R.id.filterSwitch:
                this.validationService.FilterIncomingCalls = b;
                break;

            case R.id.regexSwitch:
                for (INumberFilter f : this.validationService.getFilters())
                {
                    if (f.getClass() == RegexFilter.class)
                    {
                        ((RegexFilter) f).setEnabled(b);
                    }
                }
                break;

            case R.id.switchLocation:
                for (INumberFilter f : this.validationService.getFilters())
                {
                    if (f.getClass() == LocationFilter.class)
                    {
                        ((LocationFilter) f).setEnabled(b);
                    }
                }
                break;
        }
        /*if (compoundButton == filterSwitch)
        {
            this.validationService.FilterIncomingCalls = b;
        } else if (compoundButton == regexSwitch)
        {
            for (INumberFilter f : this.validationService.getFilters())
            {
                if (f.getClass() == RegexFilter.class)
                {
                    ((RegexFilter) f).setEnabled(b);
                }
            }
        } else if (compoundButton == locationSwitch)
        {
            for (INumberFilter f : this.validationService.getFilters())
            {
                if (f.getClass() == LocationFilter.class)
                {
                    ((LocationFilter) f).setEnabled(b);
                }
            }
        }*/
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
    {
        if (this.validationService == null)
        {
            Toast.makeText(this, "Error:service is not connected.", Toast.LENGTH_LONG).show();
            return;
        }
        for (INumberFilter f : this.validationService.getFilters())
        {
            if (f.getClass() == RegexFilter.class)
            {
                ((RegexFilter) f).setPatternString(txtRegex.getText().toString());
                Log.d("regexFilter", "set pattern");
                continue;
            }
            if (f.getClass() == LocationFilter.class)
            {
                //((LocationFilter)f)
            }
        }

    }

    @Override
    public void afterTextChanged(Editable editable)
    {

    }
}
