package com.x2v3.callblocker.filters;

import com.x2v3.callblocker.filters.INumberFilter;

/**
 *
 * Created by x on 2017/8/31.
 */

 public abstract class NumberFilterBase implements INumberFilter
{
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean enabled;
}
