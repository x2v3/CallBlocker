package com.x2v3.callblocker.filters;

/**
 * the interface which will be used to filter incoming call numbers.
 * Created by x on 2017/8/30.
 */

public interface INumberFilter {
    boolean IsBlocked(String number);
}
