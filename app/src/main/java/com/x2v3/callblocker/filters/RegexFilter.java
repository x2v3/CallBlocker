package com.x2v3.callblocker.filters;

/**
 * check whether a number is blocked by regular expressions.
 * Created by x on 2017/8/30.
 */

public class RegexFilter extends NumberFilterBase
{
    @Override
    public boolean IsBlocked(String number) {
        if(!enabled){
            return false;
        }
        boolean isMatch=number.matches(patternString);
        return isBlackListMode == isMatch ; // isBlackMode? isMatch : !isMatch
    }

    public boolean isBlackListMode() {
        return isBlackListMode;
    }

    public void setBlackListMode(boolean blackListMode) {
        isBlackListMode = blackListMode;
    }

    private boolean isBlackListMode=true;

    public String getPatternString() {
        return patternString;
    }

    public void setPatternString(String pattern) {
        this.patternString = pattern;
    }

    private String patternString="";

}
